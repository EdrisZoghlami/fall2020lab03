//Edris Zoghlami
//1935687
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTest {

	@Test
	void testX() {
		Vector3d v=new Vector3d(10,30,50);
		assertEquals(10, v.getX());
	}
	@Test
	void testY() {
		Vector3d v=new Vector3d(10,30,50);
		assertEquals(30, v.getY());
	}
	@Test
	void testZ() {
		Vector3d v=new Vector3d(10,30,50);
		assertEquals(50, v.getZ());
	}
	
	@Test
	void testMagnitude() {
		
		Vector3d v=new Vector3d(1,1,0.5);
		assertEquals(2.25, v.magnitude());
	}
	
	
	@Test
	void testDotProduct() {
		
		Vector3d v=new Vector3d(1,1,1);
		Vector3d second=new Vector3d(1,1,1);
		
		assertEquals(3, v.dotProduct(second));
	}
	
	
	@Test
	void testAdd() {
		
		Vector3d v=new Vector3d(1,1,1);
		Vector3d second=new Vector3d(1,1,1);
		assertEquals((2,2,2), v.add(second));
	}

}
