
//Edris Zoghlami
//1935687

package LinearAlgebra;

public class Vector3d {

	double x;
	double y;
	double z;
	
	
	
	public Vector3d(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double magnitude() {
		
		double magnitude = x*x+y*y+z*z;
		magnitude = Math.sqrt(magnitude);
		return magnitude;
	}
	
	public double dotProduct(Vector3d two) {
		
		double dotProduct= this.x*two.x+this.y*two.y+this.z*two.z;
		return dotProduct;
	}
	
	public Vector3d add(Vector3d two) {
		
		two.x =this.x+two.x;
		two.y =this.x+two.y;
		two.z =this.x+two.z;
		return two;
		
	}
	
	
}
